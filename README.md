# Space Invaders V2

Reformulação do jogo space invaders feito em C com Ncurses, tentando fazer melhorias de desempenho e gameplay

Como compilar:
    Utilize o comando make

Como rodar:
    Para rodar sem modificações no teclado, use o comando ./space
    Para rodar com modificações no teclado, use o comando ./run.sh